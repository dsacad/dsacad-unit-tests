#include "CppUnitTest.h"

#include "../dsacad-project/Map.h"
#include "../dsacad-project/MapTile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestmap
{
	TEST_CLASS(unittestmap)
	{
	public:
		
		TEST_METHOD(TestMapConstruction)
		{
			Map map;

			MapTile mp1;

			map.addMapTile(0, 0, &mp1);

			MapTile* mp2 = map.getMapTile(0, 0);

			// Notice on is a pointer to a map and the other is a reference to a map
			Assert::IsTrue(&mp1 == mp2, L"MapTiles should be the same.");

			// Request map tile that does not exist - should not crash
			mp2 = map.getMapTile(10, 10);
			Assert::IsTrue(nullptr == mp2, L"Request map tile that does not exist.");
		}

		TEST_METHOD(TestMapLocationDefaults)
		{
			Map map;

			int x = -15;
			int y = 18;

			map.getLocation(x, y);
			Assert::IsTrue(x == 0, L"Default X coordinate of location should be 0.");
			Assert::IsTrue(y == 0, L"Default Y coordinate of location should be 0.");
		}

		TEST_METHOD(TestMapLocationExtremesAndOrder)
		{
			Map map;

			int x = -15;
			int y = 18;

			// Testing that the order of the coordinates has been maintained in the class.
			map.setLocation(-1, 1);
			map.getLocation(x, y);
			Assert::IsTrue(x == -1, L"X coordinate of location should be -1.");
			Assert::IsTrue(y == 1, L"Y coordinate of location should be 1.");

			// Testing that the range of the datatype is working.
			map.setLocation(INT_MAX, INT_MIN);
			map.getLocation(x, y);
			Assert::IsTrue(x == INT_MAX, L"X coordinate of location should be INT_MAX.");
			Assert::IsTrue(y == INT_MIN, L"Y coordinate of location should be INT_MIN.");

			// Testing the opposite of above.
			map.setLocation(INT_MIN, INT_MAX);
			map.getLocation(x, y);
			Assert::IsTrue(x == INT_MIN, L"X coordinate of location should be INT_MIN.");
			Assert::IsTrue(y == INT_MAX, L"Y coordinate of location should be INT_MAX.");
		}

	};
}
