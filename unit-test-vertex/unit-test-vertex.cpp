#include "CppUnitTest.h"

#include "../dsacad-project/Graphics.h"

#include "../dsacad-project/Vertex.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestvertex
{
	TEST_CLASS(unittestvertex)
	{
	public:
		
		TEST_METHOD(TestVertex3DDefaultConstructor)
		{
			Vertex3D vert;

			glm::vec3 color = vert.getColor();

			Assert::IsTrue(color.r == 0.0, L"Values should match.");
			Assert::IsTrue(color.g == 0.0, L"Values should match.");
			Assert::IsTrue(color.b == 0.0, L"Values should match.");

			glm::vec3 pos = vert.getPosition();

			Assert::IsTrue(color.x == 0.0, L"Values should match.");
			Assert::IsTrue(color.y == 0.0, L"Values should match.");
			Assert::IsTrue(color.z == 0.0, L"Values should match.");
		}

		TEST_METHOD(TestVertex3DSetColorAndPosition)
		{
			Vertex3D vert;

			// These must be clamped from 1.0 to 0.0 hence the strange values
			vert.setColor(1.0f, 0.5f, 0.0f);

			glm::vec3 color = vert.getColor();

			Assert::IsTrue(color.r == 1.0, L"Values should match.");
			Assert::IsTrue(color.g == 0.5, L"Values should match.");
			Assert::IsTrue(color.b == 0.0, L"Values should match.");

			vert.setPosition(4.0f, 5.0f, 6.0f);

			glm::vec3 pos = vert.getPosition();

			// These are floating point and have to be tested with a narrow range
			Assert::IsTrue(pos.x >= 3.9999, L"Values should match.");
			Assert::IsTrue(pos.x <= 4.0001, L"Values should match.");

			Assert::IsTrue(pos.y >= 4.9999, L"Values should match.");
			Assert::IsTrue(pos.y <= 5.0001, L"Values should match.");

			Assert::IsTrue(pos.z >= 5.9999, L"Values should match.");
			Assert::IsTrue(pos.z <= 6.0001, L"Values should match.");
		}

	};
}
