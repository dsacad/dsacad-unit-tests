#include "CppUnitTest.h"

#include "../dsacad-project/Entity.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestentity
{
	TEST_CLASS(unittestentity)
	{
	public:
		
		TEST_METHOD(TestEntity)
		{
			Entity entity;

			Assert::IsTrue(entity.getName() == "", L"Default Entity name should be nil.");

			entity.setName("Scout");

			Assert::IsTrue(entity.getName() == "Scout", L"Modifed Entity name should match.");

			entity.setName("");

			Assert::IsTrue(entity.getName() == "", L"Modifed Entity name should match.");

			entity.draw(); // must implement this draw call
		}
	};
}
