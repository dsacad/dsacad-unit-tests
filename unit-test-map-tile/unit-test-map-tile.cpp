#include "CppUnitTest.h"

#include "../dsacad-project/Entity.h"
#include "../dsacad-project/MapTile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestmaptile
{
	TEST_CLASS(unittestmaptile)
	{
	public:
		
		TEST_METHOD(TestMapTile)
		{
			MapTile mt;

			Entity scout;
			Entity sentry;

			Assert::IsTrue(mt.entityCount() == 0, L"MapTile should not contain any Entities.");

			mt.addEntity(&scout);
			Assert::IsTrue(mt.entityCount() == 1, L"MapTile should contain only one Entity.");

			mt.addEntity(&sentry);
			Assert::IsTrue(mt.entityCount() == 2, L"MapTile should contain only two Entities.");

			mt.removeEntity(&scout);
			Assert::IsTrue(mt.entityCount() == 1, L"MapTile should contain only one Entity.");

			mt.draw(); // must implement this draw call
		}
	};
}
