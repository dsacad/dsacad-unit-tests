
#include "CppUnitTest.h"

#include "../dsacad-project/GraphicsComponent.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

void glGenBuffers(GLsizei n, GLuint* buffers) {
	*buffers = 1;
}


namespace unittestgraphicscomponent
{
	TEST_CLASS(unittestgraphicscomponent)
	{
	public:

		template<typename T>
		bool compareWithEpislon(T a, T b) {
			if (a - b < std::numeric_limits<double>::epsilon()) return true;
			return false;
		}

		TEST_METHOD(TestMethod1)
		{
			GraphicsComponent gc;
			glm::mat4 defaultView = gc.getMatrix();

			glm::mat4 controlView = {
			 0.853553355, -0.985598445, -0.578506112,  -0.577350259,
			 0.000000000,  1.97119689,  -0.578506112,  -0.577350259,
			-0.853553355, -0.985598445, -0.578506112,  -0.577350259,
			 0.000000000,  0.000000000,  13.6839466,   13.8564062
			};

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j], controlView[i][j]), L"Values should be equal:");
				}
			}
		}
	};
}
