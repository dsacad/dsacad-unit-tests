#include "CppUnitTest.h"

#include "..\dsacad-project\Broadcaster.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#define UNIT_TEST_GROUP_COUNT 10

namespace unittestbroadcaster
{
	class TestListener : public Listener {
	public:
		BroadcastEvent event;
		bool notified = false;
		void recieveBroadcastEvent(const BroadcastEvent _event, std::any message) {
			event = _event;
			notified = true;
		}
	};

	// we can only reset this is the hard way based on the api provided
	// keeping the api clean with proper encapsulation
	void resetBroadcaster(TestListener *classListeners) {
		for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
			for (auto j = 0; j < BROADCAST_EVENT_COUNT; ++j) {
				Broadcaster::getInstance().removeListener(static_cast<BroadcastEvent>(j), &classListeners[i]);
			}
		}
	}

	void resetListenerSettings(TestListener classListeners[]) {
		for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
			classListeners[i].notified = false;
			classListeners[i].event = BROADCAST_EVENT_COUNT; // serves as a null value
		}
	}

	// testing the test here - just to be sure our assertion is corrent
	void testListenerReset(TestListener classListeners[]) {
		for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
			Assert::IsTrue(classListeners[i].notified == false, L"All notified felds should be false.");
		}
	}

	TEST_CLASS(unittestbroadcaster)
	{
	public:
		TEST_METHOD(TestBroadcaster)
		{
			// array allows us to loop through to test easier
			TestListener classListeners[UNIT_TEST_GROUP_COUNT];
			
			// this ensures all tests stand alone
			resetListenerSettings(classListeners);
			testListenerReset(classListeners);

			// setup listener classes to listen to specified events

			Broadcaster::getInstance().addListener(BroadcastEvent::COMMAND_KEY_PRESSED, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::GRAPHICS_REDRAW_WINDOW, &classListeners[1]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_DIALOGUE, &classListeners[2]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_EXIT, &classListeners[3]);

			// notify on a single channel
			Broadcaster::getInstance().notifyAllListeners(BroadcastEvent::COMMAND_KEY_PRESSED, "message");

			// test to be sure only one (the correct one) has been notified
			for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
				if (i == 0) {
					Assert::IsTrue(classListeners[i].notified == true, L"This class should have been notified.");
				}
				else {
					Assert::IsTrue(classListeners[i].notified == false, L"This class should not have been notified.");
				}
			}

			resetListenerSettings(classListeners);

			// ensuring reset listener settings method is working.
			for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
				Assert::IsTrue(classListeners[i].notified == false, L"This class should not have been notified.");
			}

			resetBroadcaster(classListeners);

			// add all broadcast events to one listener
			// this one listener object is subscribed to all of these communication channels
			Broadcaster::getInstance().addListener(BroadcastEvent::COMMAND_KEY_PRESSED, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::GRAPHICS_REDRAW_WINDOW, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_DIALOGUE, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_EXIT, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_HELP, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_INVENTORY, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_JOURNAL, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_KEYBOARD_SHORTCUT, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_LOAD, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_MAIN, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_MAP, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_NONE, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_SAVE, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_STATS, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::MENU_TELEPORT, &classListeners[0]);
			Broadcaster::getInstance().addListener(BroadcastEvent::SOUND_CLICK, &classListeners[0]);

			// loop through the same number of channels as the unit test group count
			for (auto i = 0; i < UNIT_TEST_GROUP_COUNT; ++i) {
				// send notification to all listeners that this broadcast event has occured
				Broadcaster::getInstance().notifyAllListeners(static_cast<BroadcastEvent>(i), "message");
				// loop through all the broadcast event channnels this listener is subscribed to
				for (auto j = 0; j < UNIT_TEST_GROUP_COUNT; ++j) {
					// verify the correct channel has been notified or not
					if (i == j) {
						Assert::IsTrue(classListeners[0].event == static_cast<BroadcastEvent>(j), L"This channel should have been notified.");
					}
					else {
						Assert::IsFalse(classListeners[0].event == static_cast<BroadcastEvent>(j), L"This channel should not have been notified.");
					}
				}
				// reset the status of the listeners to a known state
				resetListenerSettings(classListeners);
			}
		}
	};
}
