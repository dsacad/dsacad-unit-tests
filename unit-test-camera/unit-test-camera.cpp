#include "CppUnitTest.h"

#include <vector>
#include "glm.hpp"
#include "ext.hpp"
#include "gtx/transform.hpp"
#include "gtc/matrix_transform.hpp"

#include "../dsacad-project/Camera.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestcamera
{
	template<typename T>
	bool compareWithEpislon(T a, T b) {
		if (a - b < std::numeric_limits<double>::epsilon()) return true;
		return false;
	}

	TEST_CLASS(unittestcamera)
	{
	public:
		
		TEST_METHOD(TestCameraConstructor)
		{
			Camera myTestCamera(200,100);

			glm::mat4 controlView = {
				 0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  1.97119689,  -0.578506112,  -0.577350259,
				-0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  0.000000000,  13.6839466,   13.8564062
			};

			glm::mat4 defaultView = myTestCamera.getCameraMatrix();

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j],controlView[i][j]), L"Values should be equal:");
				}
			}
		}

		TEST_METHOD(TestCameraSetPosition)
		{
			Camera myTestCamera(1920, 1080);

			glm::mat4 controlView = {
				 1.28830743,  -0.408076584,    -0.267796308,  -0.267261237,
				 0.000000000,  2.04038286,     -0.535592616,  -0.534522474,
				-0.429435819, -1.22422969,     -0.803388834,  -0.801783681,
				 0.000000000,  2.87796666e-07,  3.54894781,    3.74165726
			};

			myTestCamera.setPosition(glm::vec3(1.0f, 2.0f, 3.0f));

			glm::mat4 defaultView = myTestCamera.getCameraMatrix();

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j], controlView[i][j]), L"Values should be equal:");
				}
			}
		}

		TEST_METHOD(TestCameraSetTarget)
		{
			Camera myTestCamera(800, 600);

			glm::mat4 controlView = {
				 1.47339487,  -0.802758157,  -0.477685720,  -0.476731300,
				 0.000000000,  1.98013687,   -0.573222816,  -0.572077513,
				-1.05242491,  -1.12386143,   -0.668759942,  -0.667423785,
				-3.36775899,  -0.428138971,  13.5571470,    13.7298603
			};

			myTestCamera.setTarget(glm::vec3(3.0f, 2.0f, 1.0f));

			glm::mat4 defaultView = myTestCamera.getCameraMatrix();

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j], controlView[i][j]), L"Values should be equal:");
				}
			}
		}

		TEST_METHOD(TestCameraSetTargetThenReset)
		{
			Camera myTestCamera(200, 100);

			glm::mat4 controlView = {
				 0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  1.97119689,  -0.578506112,  -0.577350259,
				-0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  0.000000000,  13.6839466,   13.8564062
			};

			myTestCamera.setTarget(glm::vec3(3.0f, 2.0f, 1.0f));
			myTestCamera.resetCamera();

			glm::mat4 defaultView = myTestCamera.getCameraMatrix();

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j], controlView[i][j]), L"Values should be equal:");
				}
			}
		}

		TEST_METHOD(TestCameraSetPositionThenReset)
		{
			Camera myTestCamera(200, 100);

			glm::mat4 controlView = {
				 0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  1.97119689,  -0.578506112,  -0.577350259,
				-0.853553355, -0.985598445, -0.578506112,  -0.577350259,
				 0.000000000,  0.000000000,  13.6839466,   13.8564062
			};

			myTestCamera.setTarget(glm::vec3(3.0f, 2.0f, 1.0f));
			myTestCamera.resetCamera();

			glm::mat4 defaultView = myTestCamera.getCameraMatrix();

			// comparing each value to make debugging easier
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					Assert::IsTrue(compareWithEpislon(defaultView[i][j], controlView[i][j]), L"Values should be equal:");
				}
			}
		}
	};
}
