#include "CppUnitTest.h"

#include "../dsacad-project/ModelTokenizer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestmodeltokenizer
{
	TEST_CLASS(unittestmodeltokenizer)
	{
	public:
		TEST_METHOD(NullInput)
		{
			// should indicate at end of input and
			// also should not crash due to null pointer dereference
			char *comment = nullptr;
			ModelTokenizer tokenizer(comment);
			Logger::WriteMessage("Should recognize empty input.");
			Assert::IsTrue(tokenizer.atEnd() == true, L"Should recognize empty input at being at end.");
		}

		TEST_METHOD(CommentAtEndOfFile)
		{
			// comment is at end of file
			char data[] = "#This is a comment\0";
			ModelTokenizer tokenizer(data);
			Assert::IsTrue(tokenizer.atEnd() == false, L"Should recognize input not at being at end.");
			tokenizer.next(); // should skip over comment
			Assert::IsTrue(tokenizer.atEnd() == true, L"Nothing left past end of comment.");
		}

		TEST_METHOD(CommentWithMoreTokens)
		{
			// comment with a token on next line
			char data[] = "#This is a comment\nvec3\0"; 
			ModelTokenizer tokenizer(data);
			tokenizer.next(); // should skip over comment
			Assert::IsTrue(tokenizer.symbol() == 'v', L"Should be pointing at the 'v' in the next symbol.");
		}

		TEST_METHOD(DetermineKeywordsCorrectly)
		{
			char vec2data[] = "vec2 0.0 0.0 0.0\n";
			ModelTokenizer vec2tokenizer(vec2data);
			Assert::IsTrue(vec2tokenizer.keyWord() == ModelTokenizer::KeyWord::VEC2, L"Should find VEC2 keyword.");

			char vec3data[] = "vec3 0.0 0.0 0.0\n";
			ModelTokenizer vec3tokenizer(vec3data);
			Assert::IsTrue(vec3tokenizer.keyWord() == ModelTokenizer::KeyWord::VEC3, L"Should find VEC3 keyword.");

			char vec4data[] = "vec4 0.0 0.0 0.0\n";
			ModelTokenizer vec4tokenizer(vec4data);
			Assert::IsTrue(vec4tokenizer.keyWord() == ModelTokenizer::KeyWord::VEC4, L"Should find VEC4 keyword.");

			char mat2data[] = "mat2 0.0 0.0 0.0\n";
			ModelTokenizer mat2tokenizer(mat2data);
			Assert::IsTrue(mat2tokenizer.keyWord() == ModelTokenizer::KeyWord::MAT2, L"Should find MAT2 keyword.");

			char mat3data[] = "mat3 0.0 0.0 0.0\n";
			ModelTokenizer mat3tokenizer(mat3data);
			Assert::IsTrue(mat3tokenizer.keyWord() == ModelTokenizer::KeyWord::MAT3, L"Should find MAT3 keyword.");

			char mat4data[] = "mat4 0.0 0.0 0.0\n";
			ModelTokenizer mat4tokenizer(mat4data);
			Assert::IsTrue(mat4tokenizer.keyWord() == ModelTokenizer::KeyWord::MAT4, L"Should find MAT4 keyword.");
		}

		TEST_METHOD(GetFloatingPointValuesCorrectly)
		{
			// "regular" number
			char data1[] = "1.2 3.4";
			ModelTokenizer tokenizer1(data1);
			Assert::IsTrue(tokenizer1.floatValue() == 1.2f, L"Floating point value should be accurate.");

			// zeroed number
			char data2[] = "0.0 3.4";
			ModelTokenizer tokenizer2(data2);
			Assert::IsTrue(tokenizer2.floatValue() == 0.0f, L"Floating point value should be accurate.");

			// negative number
			char data3[] = "-10.0 3.4";
			ModelTokenizer tokenizer3(data3);
			Assert::IsTrue(tokenizer3.floatValue() == -10.0f, L"Floating point value should be accurate.");

			// negative number with post 'f'
			char data4[] = "-20.0f 3.4";
			ModelTokenizer tokenizer4(data4);
			Assert::IsTrue(tokenizer4.floatValue() == -20.0f, L"Floating point value should be accurate.");

			// positive number with post 'f'
			char data5[] = "30.0f 3.4";
			ModelTokenizer tokenizer5(data5);
			Assert::IsTrue(tokenizer5.floatValue() == 30.0f, L"Floating point value should be accurate.");
		}

		TEST_METHOD(GetNextTokenCorrectly)
		{
			// several tokens in a series
			char data[] = "# This is a comment\nvec3 0.1 2.3 4.5\nmat2 6 7 8 9\0";
			ModelTokenizer tokenizer(data);
			tokenizer.next(); // skipping comments
			Assert::IsTrue(tokenizer.symbol() == 'v', L"Should point to vec3");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '0', L"Should point to beginning of first float '0.1'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '2', L"Should point to beginning of second float '2.3'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '4', L"Should point to beginning of third float '4.5'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == 'm', L"Should point to beginning of 'mat2'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '6', L"Should point to beginning of first float '6'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '7', L"Should point to beginning of second float '7'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '8', L"Should point to beginning of third float '8'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '9', L"Should point to beginning of forth float '9'.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '\0', L"Should be at end.");
			tokenizer.next();
			Assert::IsTrue(tokenizer.symbol() == '\0', L"Should not crash reading memory out of bounds.");
		}

		TEST_METHOD(GetMultipleCommentsCorrectly)
		{
			char data[] = "# This is a comment\n# Another comment\nvec3 0.1 2.3 4.5\nmat2 6 7 8 9\0";
			ModelTokenizer tokenizer(data);
			tokenizer.next(); // skipping multiple comments
			Assert::IsTrue(tokenizer.symbol() == 'v', L"Should point to vec3");
		}

		TEST_METHOD(CheckTokenTypes)
		{
			char dataVec3[] = "vec3 0 0 9";
			ModelTokenizer tokenizer1(dataVec3);
			Assert::IsTrue(tokenizer1.tokenType() == ModelTokenizer::TokenType::KEYWORD, L"Should find keyword as token type.");
			char dataMat4[] = "mat4 1 2 3 4 5 6 7 8 9  0 1 2 3 4 5 6";
			ModelTokenizer tokenizer2(dataMat4);
			Assert::IsTrue(tokenizer2.tokenType() == ModelTokenizer::TokenType::KEYWORD, L"Should find keyword as token type.");

			// Testing for floating point numbers
			char dataFloat1[] = "1";
			ModelTokenizer tokenizer3(dataFloat1);
			Assert::IsTrue(tokenizer3.tokenType() == ModelTokenizer::TokenType::NUMBER, L"Should find number as token type.");

			char dataFloat2[] = "+0.0000001";
			ModelTokenizer tokenizer4(dataFloat2);
			Assert::IsTrue(tokenizer4.tokenType() == ModelTokenizer::TokenType::NUMBER, L"Should find number as token type.");

			char dataFloat3[] = ".0";
			ModelTokenizer tokenizer5(dataFloat3);
			Assert::IsTrue(tokenizer5.tokenType() == ModelTokenizer::TokenType::NUMBER, L"Should find number as token type.");

			char dataFloat4[] = "-0.0";
			ModelTokenizer tokenizer6(dataFloat4);
			Assert::IsTrue(tokenizer6.tokenType() == ModelTokenizer::TokenType::NUMBER, L"Should find number as token type.");

			char dataFloat5[] = "3.141592654f";
			ModelTokenizer tokenizer7(dataFloat3);
			Assert::IsTrue(tokenizer7.tokenType() == ModelTokenizer::TokenType::NUMBER, L"Should find number as token type.");

			char dataString1[] = "hi"; // friendly string
			ModelTokenizer tokenizer8(dataString1);
			Assert::IsTrue(tokenizer8.tokenType() == ModelTokenizer::TokenType::STRING, L"Should find string as token type.");

			char dataString2[] = "a"; // short string
			ModelTokenizer tokenizer9(dataString2);
			Assert::IsTrue(tokenizer9.tokenType() == ModelTokenizer::TokenType::STRING, L"Should find string as token type.");

			char dataString3[] = "supercalifragilisticexpialidocious"; // looooooong string
			ModelTokenizer tokenizer0(dataString3);
			Assert::IsTrue(tokenizer0.tokenType() == ModelTokenizer::TokenType::STRING, L"Should find string as token type.");
		}


	};
}
